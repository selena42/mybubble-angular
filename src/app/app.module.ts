import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ChatPageComponent } from './chat-page/chat-page.component';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import { ConvoComponent } from './convo/convo.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from './convo/filter.pipe';
import { OrderByPipe } from './convo/order-by.pipe';
import { FilterMsgPipe } from './chat-page/filter-msg.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ChatPageComponent,
    ConvoComponent,
    FilterPipe,
    OrderByPipe,
    FilterMsgPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
