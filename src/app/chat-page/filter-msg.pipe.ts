import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'filterMsg'
})

//this pipe is used here to filter the messages according to the database
//on the other hand the database should be structured differently 

export class FilterMsgPipe implements PipeTransform {

  transform(collection: any, term: any): any {
    if (term === undefined) return collection;
    return collection.filter(function (item: { name: string; }) {
        return item.name.toLowerCase().includes(term.toLowerCase());
      })
  }
 

}
