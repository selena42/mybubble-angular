import { Component, OnInit, Input , VERSION, ViewChild, ElementRef} from '@angular/core';
import { ChatService } from '../services/chat.service';
import {FormGroup,FormControl} from '@angular/forms';
import {NavigationExtras} from '@angular/router';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ConvoService} from '../services/convo.service';

@Component({
  moduleId: 'module.Id',
  selector: 'app-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.css']
})
export class ChatPageComponent implements OnInit {

  
  

  
//get the input message value and display it on click without reloading the page
  messages=[];
  inputValue:string;
  getVal(val:any){
    // console.warn(val);
    if(!(val=="")){
    this.messages.push(val);
    
    }
    this.inputValue= val;
  }

  constructor(private chat:ChatService, private conv: ConvoService) { }

 collection={};
collection2={};

//formgroup to save the data
addMessage= new FormGroup({
  name: new FormControl(''),
  message: new FormControl('')
  
})

passName:string;
// sentElement:any;
  ngOnInit(): void {
    //to display the messages already sent and saved in the database
    this.chat.getList().subscribe((result)=>{
      this.collection=result;
    })
    this.chat.getSent().subscribe((result2)=>{
      this.collection2=result2;
    })

    // getting data on click from the other component
   this.passName= this.conv.getData();
  //  this.sentElement=document.getElementById('sent');
  // if(this.sentElement=="" || this.sentElement==null){
  //   alert('hi dud');
  // }
 

  }
  //save the formgroup-new message to the database with a condition (the message can't be empty)
chatting(){
    this.addMessage.patchValue({  //I had to patch the value i couldnt insert it directly (formgroup thing)
      name: this.passName, 
  
    });
    if(!(this.inputValue==""||this.inputValue==null)){ //i have to target the message in the condition
      this.chat.saveMessage(this.addMessage.value).subscribe((res)=>{
        this.addMessage.reset({});
 
      })
    }
}



}
 