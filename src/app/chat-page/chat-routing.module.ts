import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {ConvoComponent } from '../convo/convo.component';

const route : Routes = [
  
  {
    component: ConvoComponent,
    path : 'convo'
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(route)
  ],
  exports: [RouterModule]
  
})
export class ChatRoutingModule { }
