import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

//this service will get the data inside the database in order to display it
//and will also save data (messages) in the database
export class ChatService {
  url='http://localhost:3000/conversation';
  url2='http://localhost:3000/sentMessages';
  

  constructor(private http:HttpClient) { }




  getList(){
    return this.http.get(this.url);
  }
  getSent(){
    return this.http.get(this.url2);
  }
  saveMessage(data: any){
     return  this.http.post(this.url,data);
  }
}
