import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConvoComponent} from '../convo/convo.component';

@Injectable({
  providedIn: 'root'
})

//this service will also get data from the database 
//set the data on click sent by this component (convo) to the other (chat)

export class ConvoService {
  url='http://localhost:3000/users';
  passName:string;
  
  constructor(private http:HttpClient) { }

  setData(data:any){
    this.passName=data
  }
  getData(){
    return this.passName
  }

  getList(){
    return this.http.get(this.url);
  }
  // saveMessage(data: any){
  //    return  this.http.post(this.url,data);
  // }
}