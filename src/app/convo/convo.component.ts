import { Component, OnInit } from '@angular/core';
import {ConvoService} from '../services/convo.service';


@Component({
  selector: 'app-convo',
  templateUrl: './convo.component.html',
  styleUrls: ['./convo.component.css']
})
export class ConvoComponent implements OnInit {
collection={}
  constructor(private conv:ConvoService) { }

  passName:any;

  getName(value:string){
   //passing value to the other component on click
    this.passName=value;
    this.passName= this.conv.setData(this.passName);
    
  }

  ngOnInit(): void {
    //display the users in the convo page
    this.conv.getList().subscribe((result)=>{
      this.collection=result;
    })
    
  }
  
}
