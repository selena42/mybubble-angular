import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
//this pipe is a search filter
export class FilterPipe implements PipeTransform {

  transform(collection: any, term: any): any {
    if (term === undefined) return collection;
    return collection.filter(function(item){
        return item.name.toLowerCase().includes(term.toLowerCase());
    })
  }

}
