import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
//this pipe will order the names of the users alphabetically 
export class OrderByPipe implements PipeTransform {

  transform(array: any, field: any): any[] {
    array.sort((a: any, b: any) => {
      if (a[field] < b[field]) {
        return -1;
      } else if (a[field] > b[field]) {
        return 1;
      } else {
        return 0;
      }
    });
    return array;

}
}