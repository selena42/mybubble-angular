import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {ChatPageComponent } from './chat-page/chat-page.component';
import {ConvoComponent } from './convo/convo.component';

const routes : Routes = [
  {
    component: ChatPageComponent,
    path : 'chatpage'
  },
  {
    component: ConvoComponent,
    path : 'convo'
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
